import pytest
from aurora.aurora import create_app

def test_route_url_index():
    appi = create_app("testIndex")
    with appi.test_client() as test:
        response = test.get('/')
        assert response.status_code == 200

def test_route_video_feed():
    appi = create_app("testVideo")
    with appi.test_client() as test:
        response = test.get('video_feed')
        assert response._status == '200 OK'

def test_index_history():
    appi = create_app("testVideo")
    with appi.test_client() as test:
        response = test.get('/')
        assert len(response.history) == 0

def test_video_history():
    appi = create_app("testVideoHistory")
    with appi.test_client() as test:
        response = test.get('/video_feed')
        assert len(response.history) == 0
