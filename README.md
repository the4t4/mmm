# Aurora

Listening to music which reflects your emotions can boost your mood and cognitive skills (Franco, F., et al. 2014 [Psychology of Music](https://doi.org/10.1177/0305735614548500)). Aurora is a project made just for that! She will be able to see and detect your mood in real time and play music accordingly.

[See the AI in action!](https://aurora7.herokuapp.com/)
P.S. the emotion recognition solution does not work over remote server yet.

## Quickstart

### Prerequisite
- python 3.9
- pip

### Install dependencies
May need admin rights
```bash
python -m pip install -r requirements.txt
```

### Set env var
Using a bash terminal, execute
```bash
export FLASK_APP=path/to/app.py
```
Using a powershell terminal, execute
```powershell
$env:FLASK_APP =path/to/app.py
```

### Deploy locally
```bash
flask run --no-restart
```
Then go to https://127.0.0.1:5000


### Running tests
```bash
flask test
```

## License

Copyright (C) 2021  Aurora Team

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program, see [License](/LICENSE) for more details. If not, see <https://www.gnu.org/licenses/>.

