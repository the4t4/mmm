from fer import FER
import cv2
import numpy as np

class EmotionRecognizer:
    def __init__(self):
        self.emotion_recognizer = FER(min_neighbors=3)
        self.video_capture = cv2.VideoCapture(0)
        self.output_frame = np.zeros((1,1,3), np.uint8)

        # Haar cascade for face classification
        self.face_classifier = cv2.CascadeClassifier(
            cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

    def __del__(self):
        self.video_capture.release()

    def detect_emotion(self):
        """ Classifies the detection of emotion and it registers the output into six categories
        namely, "surprise", "fear", "neutral", "happy", "sad", "anger", and "disgust". Every emotion is calculated,
        and the output is put on a scale of 0 to 1."""
        while True:
            # Grab a single frame of video and setting the Region Of Interest (ROI)
            (ret, frame) = self.video_capture.read()
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            faces = self.face_classifier.detectMultiScale(gray, 1.3, 5)

            (emotion, score) = self.emotion_recognizer.top_emotion(frame)

            for (x, y, w, h) in faces:

                cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 144, 30), 2)

                if emotion is not None:
                    text = emotion + ' ' + str(int(score*100)) + '%'
                    position = (x, y-10)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    fontScale = 1.5
                    color = (255, 144, 30)
                    thickness = 2

                    cv2.putText(frame, text, position, font,
                                fontScale, color, thickness)

            self.output_frame = frame

    def generate(self):
        """Generates the video feed"""
        while True:
            if self.output_frame is None:
                continue

            (flag, encodedImage) = cv2.imencode(".jpg", self.output_frame)

            if not flag:
                continue

            yield(b'--frame\r\n' b'Content-Type: image/jpeg\r\n\r\n' +
                  bytearray(encodedImage) + b'\r\n')
