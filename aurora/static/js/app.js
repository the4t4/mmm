const giantAurora = document.querySelector('h1')
const tinyAurora = document.querySelector('#tinyAurora')
const video = document.querySelector('video')
const webcamFeed = document.querySelector('#webcamFeed')


giantAurora.style.cursor = "pointer";
tinyAurora.style.cursor = "pointer";

giantAurora.addEventListener('click',() => {
    animate()
})

tinyAurora.addEventListener('click',() => {
    animate()
})

function animate(){
    video.classList.toggle('active')

    giantAurora.classList.toggle('active')

    setTimeout(function() {
        webcamFeed.hidden = !webcamFeed.hidden
        if(giantAurora.style.display === "none"){
            giantAurora.style.display = "block";
            tinyAurora.style.display = "none";
        } else {
            giantAurora.style.display = "none";
            tinyAurora.style.display = "block";
        }
    }, 250); 

}