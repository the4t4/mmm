import os
import click
from flask.cli import with_appcontext


HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')



@click.command(name='test')
@with_appcontext
def test():
    """Run the tests."""
    import pytest
    print(TEST_PATH)
    rv = pytest.main([TEST_PATH, '--verbose'])
    exit(rv)