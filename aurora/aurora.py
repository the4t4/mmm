from aurora.emotion_recognition.emotion_recognizer import EmotionRecognizer

from aurora import commands
from flask import Response
from flask import Flask
from flask import render_template
import threading

class Aurora():

    def __init__(self, name="aurora", *args, **kw):
        """ The flask object implements a WSGI application and acts as the central object.
        It is passed the name of the module or package of the application. 
        Once it is created it will act as a central registry for the view functions,
        the URL rules, template configuration and more. """
        self.app = Flask(name, *args, **kw)
        self.app.cli.add_command(commands.test)
        self.er = EmotionRecognizer()
        self.route_urls()
        self.connect_backend()

    def run(self, host=None, port=None, debug=None):
        self.app.run(host=host, port=port, debug=debug, use_reloader=False)

    def connect_backend(self):
        @self.app.before_first_request
        def before_first_request():
            t = threading.Thread(target=self.er.detect_emotion)
            t.daemon = True
            t.start()
            #music generator will be threaded here as well

    def route_urls(self):
        @self.app.route("/")
        def index():
            return render_template("index.html")

        @self.app.route("/video_feed")
        def video_feed():
            return Response(self.er.generate(),
                            mimetype="multipart/x-mixed-replace; boundary=frame")

def create_app(name):
    return Aurora(name, template_folder="aurora/templates", static_folder="aurora/static").app
